var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Ayutec' });
});

router.get('/form', function(req, res, next) {
  res.render('form', { title: 'Ayutec' });
});

router.post('/form', function(req, res, next) {
  res.send(req.body);
});

router.get('/login', function(req, res, next) {
  res.render('login', { title: 'Ayutec' });
});

router.get('/register', function(req, res, next) {
  res.render('register', { title: 'Ayutec' });
});

router.post('/register', function(req, res, next) {
  res.send(req.body);
});

router.get('/works', function(req, res, next) {
  res.render('works', { title: 'Ayutec' });
});

router.get('/works/:id', function(req, res, next) {
  Tecnico.findById(id).then(function(tecnico) {
    console.log(tecnico)
    })
});


module.exports = router;
