const { DataTypes } = require("sequelize");
module.exports = (sequelize) => {
    sequelize.define('trabajo', {

        titulo: {
            type: DataTypes.STRING,
            allowNull: false
          },
          descripcion: {
            type: DataTypes.TEXT,
            allowNull: false
          },
          imagen: {
            type: DataTypes.STRING
          },
          estado: {
            type: DataType.BOOLEAN
          },
          presupuesto: {
            type: DataTypes.FLOAT
          },
          fechaEntrega: {
            type: DataTypes.DATE
          }
          
    })
};